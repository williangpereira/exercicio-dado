package br.com.itau;

import java.util.Random;

public class SorteioUm {

    public static void main(String[] args) {

        int somadorTotal = 0;

        for (int j = 0; j < 3; j++){

            Random random = new Random();

            int soma = 0;

            for (int i = 0; i < 3 ; i ++) {

                int resultadoUm = random.nextInt(6) + 1;

                System.out.print(resultadoUm);
                System.out.print(", ");

                soma += resultadoUm;

            }

            System.out.println(soma);

            somadorTotal += soma;

        }

        System.out.println("soma de todos: " + somadorTotal);

    }

}
